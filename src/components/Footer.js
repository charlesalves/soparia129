import React from 'react'

export const Footer = () => {
  return (
    <div className="footer text-center px-3 pt-3">
      <div className="row logo">
        <div className="col-12">
          <img src="/imgs/Logo-transparent.png" alt="" height="150" width="150"/>
          Soparia 129
        </div>
      </div>
      <div className="row social mt-5">
        <a href="https://wa.me/5581981597588" target="_blank" rel="noreferrer">
          <i class="bi bi-whatsapp"></i> (81) 98159-7588
        </a>
      </div>
      <div className="row social">
        <a href="https://instagram.com/soparia129" target="_blank" rel="noreferrer">
          <i class="bi bi-instagram"></i> @Sopraria129
        </a>
      </div>
    </div>
  )
}
