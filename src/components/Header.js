import React from 'react'

export const Header = () => {
  return (
    <nav className="navbar navbar-dark sopa-header">
      <div className="container-fluid">
        <span className="navbar-brand mb-0 h1">
          <img src="/imgs/icon-color2.png" alt="" width="30" height="24" className="d-inline-block align-top"/>
          Soparia 129
        </span>
      </div>
    </nav>
  )
}
