import React from 'react'

import './App.css'
import { Header } from './Header'
import { MasterHeader } from './MasterHeader'
import { Menu } from './Menu'
import { Footer } from './Footer'

export const App = () => {
  return (
    <>
      <Header/>
      <MasterHeader/>
      <div className="container">
        <Menu/>
      </div>
      <Footer/>
    </>
  )
}
